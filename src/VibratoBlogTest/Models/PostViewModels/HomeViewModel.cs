﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VibratoBlogTest.Models.PostViewModels
{
    public class HomeViewModel
    {
        public bool IsSignedIn { get; set; }
        public IList<Post> Posts { get; set; }
    }
}
