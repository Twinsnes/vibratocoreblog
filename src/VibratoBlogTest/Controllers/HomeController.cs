﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VibratoBlogTest.Data;
using VibratoBlogTest.Models;
using VibratoBlogTest.Models.PostViewModels;

namespace VibratoBlogTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signinManager;

        public HomeController(ApplicationDbContext context, SignInManager<ApplicationUser> signinManager)
        {
            _context = context;
            _signinManager = signinManager;
        }

        public IActionResult Index()
        {
            var model = new HomeViewModel();
            model.IsSignedIn = _signinManager.IsSignedIn(User);
            model.Posts = _context.Posts.ToList();
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        [Authorize]
        [Route("Home/Create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [Authorize]
        [Route("Home/Create")]
        [HttpPost]
        public async Task<IActionResult> Create(PostViewModel postViewModel)
        {
            if (ModelState.IsValid)
            {

                var post = new Post
                {
                    Title = postViewModel.Title,
                    Content = postViewModel.Content
                };

                _context.Posts.Add(post);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Home");
            }

            return View(postViewModel);
        }
    }
}
